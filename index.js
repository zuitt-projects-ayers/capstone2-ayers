// import dependencies
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

// import routes: const thingRoutes = require('./routes/thingRoutes');
const userRoutes = require('./routes/userRoutes');
const productRoutes = require('./routes/productRoutes');

// set up server
const port = 8000;
const app = express();

mongoose.connect("mongodb+srv://admin-ayers:admin169@ayers-169.mgg6c.mongodb.net/capstone2-ayers?retryWrites=true&w=majority", {
	useNewUrlParser : true,
	useUnifiedTopology : true
});

let db = mongoose.connection;
db.on('error', console.error.bind(console, 'Connection Error.'));
db.once('open', () => console.log('Connected to MongoDB.'));

// middlewares
app.use(express.json());
app.use(cors());

// used routes (middlewares): app.use('/things', thingRoutes);
app.use('/users', userRoutes);
app.use('/products', productRoutes);

app.listen(port, () => console.log(`Server is running at port ${port}.`));