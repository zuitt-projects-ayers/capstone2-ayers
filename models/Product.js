// import mongoose
const mongoose = require('mongoose');

// create data model
let userSchema = new mongoose.Schema({

	name : {
		type : String,
		required : [true, "Product Name is required."]
	},

	description : {
		type : String,
		required : [true, "Product Description is required."]
	},

	price : {
		type : Number,
		required : [true, "Product Price is required."]
	},

	isActive : {
		type : Boolean,
		default : true
	},

	publishedOn : {
		type : Date,
		default : new Date()
	},

	orders : [
		{
			userId : {
				type : String,
				required : [true, "User ID is required."]
			},

			productId : {
				type : String,
				required : [true, "Product ID is required."]
			},

			totalAmount : {
				type : Number,
				required : [true, "Amount ordered is required."]
			},

			purchasedOn : {
				type : Date,
				default : new Date()
			}
		}
	]
});

// export
module.exports = mongoose.model("Product", userSchema);