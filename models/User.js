// import mongoose
const mongoose = require('mongoose');

// create data model
let userSchema = new mongoose.Schema({

	firstName : {
		type : String,
		required : [true, "First Name is required."]
	},

	lastName : {
		type : String,
		required : [true, "Last Name is required."]
	},

	email : {
		type : String,
		required : [true, "Email is required."]
	},

	password : {
		type : String,
		required : [true, "Password is required."]
	},

	mobileNo : {
		type : String,
		required : false
	},

	isAdmin : {
		type : Boolean,
		default : false
	},	

	orders : [
		{
			userId : {
				type : String,
				required : [true, "User ID is required."]
			},

			productId : {
				type : String,
				required : [true, "Product ID is required."]
			},

			totalAmount : {
				type : Number,
				required : [true, "Amount ordered is required."]
			},

			purchasedOn : {
				type : Date,
				default : new Date()
			}
		}
	]
});

// export
module.exports = mongoose.model("User", userSchema);