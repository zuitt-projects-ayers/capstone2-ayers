// import bcrypt module
const bcrypt = require('bcrypt');

// import User and Product models
const User = require("../models/User");
const Product = require("../models/Product");

// import auth module
const auth = require("../auth")

// Controllers

// Register User
module.exports.registerUser = (req, res) => {

	// log info to check
	console.log(req.body);

	// encrypt password
	const hashedPW = bcrypt.hashSync(req.body.password, 10);

	// check if email is already taken
	User.findOne({email : req.body.email})
	.then(result => {

		if (result !== null && result.email === req.body.email) {
			console.log("Email has already been taken.")
			return res.send('Email has already been taken.');

		} else {
			// create new user
			let newUser = new User({
				firstName : req.body.firstName,
				lastName : req.body.lastName,
				email : req.body.email,
				password : hashedPW,
				mobileNo : req.body.mobileNo
			});

			newUser.save()
			.then(user => res.send(user))
			.catch(err => res.send(err));
		};
	})
	.catch(err => res.send(err));
};


// Login User
module.exports.loginUser = (req, res) => {

	// log info to check
	console.log(req.body);

	// find a user via email
	User.findOne({email : req.body.email})
	.then(foundUser => {

		if (foundUser === null) {
			return res.send("User does not exist.");

		} else {
			// check if password is correct for user
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, foundUser.password);

			if (isPasswordCorrect) {
				return res.send({accessToken : auth.createAccessToken(foundUser)});

			} else {
				return res.send("Password is incorrect.");
			}
		}
	}).catch(err => res.send(err));
};


// Set User as Admin (Admin Only)
module.exports.setAdmin = (req, res) => {

	// log info to check
	console.log(req.user.id + req.user.email);
	console.log(req.params.id + req.user.email);


	// create updates object
	let updates = {
		isAdmin : true
	};

	// find by id and update
	User.findByIdAndUpdate(req.params.id, updates, {new:true})
	.then(updatedUser => res.send(updatedUser))
	.catch(err => res.send(err));
};


// Get All Users (Admin Only)
module.exports.getAllUsers = (req, res) => {

	// log info to check
	console.log(req.user)

	// find all users (no criteria)
	User.find({})
	.then(result => res.send(result))
	.then(err => res.send(err));
};

// Get Single User (Admin Only)
module.exports.getSingleUser = (req, res) => {

	// log info to check
	console.log(req.user)

	// find all users (no criteria)
	User.findById(req.params.id)
	.then(result => res.send(result))
	.then(err => res.send(err));
};

// Get User's Info
module.exports.getInfo = (req, res) => {

	// log info to check
	console.log(req.user)

	// find by id
	User.findById(req.user.id)
	.then(result => res.send(result))
	.then(err => res.send(err));
};


// Update User's Info
module.exports.updateUserInfo = (req, res) => {

	// log info to check
	console.log(req.user)

	// creates updates object
	let updates = {
		firstName : req.body.firstName,
		lastName : req.body.lastName,
		password : req.body.password,
		mobileNo : req.body.mobileNo
	}

	// find by id and update
	User.findByIdAndUpdate(req.user.id, updates, {new:true})
	.then(updatedUser => res.send(updatedUser))
	.catch(err => res.send(err));
};


// Make an Order (Regular User Only)
module.exports.makeOrder = async (req, res) => {

	// log info to check
	console.log(req.user.id);
	console.log(req.body.productId);
	console.log(req.body.totalAmount);

	// check if user is an admin
	if (req.user.isAdmin) {
		return res.send("Action Forbidden for Admin.");
	};

	// isUserUpdated
	let isUserUpdated = await User.findById(req.user.id)
	.then(user => {

		// log info to check
		console.log(user);

		// create an object which will contain the orderId of the product ordered
		let newOrder = {
			userId : req.user.id,
			productId : req.body.productId,
			totalAmount : req.body.totalAmount
		};
		user.orders.push(newOrder);

		// save
		return user.save()
		.then(user => true)
		.catch(err => err.message);
	});

	// check if user is updated
	if (isUserUpdated !== true) {
		return res.send({message : isUserUpdated});
	}

	// isProductUpdated
	let isProductUpdated = await Product.findById(req.body.productId)
	.then(product => {

		// log info to check
		console.log(product);

		// create an object which will contain the userId of the user who made the order
		let orderer = {
			userId : req.user.id,
			productId : req.body.productId,
			totalAmount : req.body.totalAmount
		};
		product.orders.push(orderer);

		// save
		return product.save()
		.then(product => true)
		.catch(err => err.message);
	});

	// check if product is updated
	if (isProductUpdated !== true) {
		return res.send({message : isProductUpdated});
	}

	if (isUserUpdated && isProductUpdated) {
		return res.send({message : 'Order Successful!'});
	}
};


// Get User's Orders
module.exports.getOrders = (req, res) => {

	// find by id
	User.findById(req.user.id)
	.then(user => {

		// log info to check
		console.log(user);

		// return user.orders
		return res.send(user.orders);
	})
	.catch(err => res.send(err));
};


// Get All Orders (Admin Only)
module.exports.getAllOrders = (req, res) => {

	// find all users (no criteria)
	User.find({})
	.then(allUsers => {

		// log info to check
		console.log(allUsers);
		console.log(allUsers.length);

		allOrders = []

		for (let i = 0; i < allUsers.length; i++) {
			console.log(`\n${i}\n`);
			console.log(allUsers[i]);
			if (allUsers[i].orders.length > 0) {
				allOrders.push(allUsers[i].email);
				allOrders.push(allUsers[i].orders);
			}
		};
		
		res.send(allOrders);
	})
	.catch(err => res.send(err));
};


// Cancel User's Order (not working)
module.exports.cancelOrder = async (req, res) => {

	// log info to check
	console.log(req.user.id);
	console.log(req.body.id)

	// isUserUpdated
	let isUserUpdated = await User.findById(req.user.id)
	.then(user => {

		// log info to check
		console.log(user);

		// create an object which will contain the orderId of the product ordered
		for (let i = 0; i < user.orders.length; i++) {
			console.log(`\n${i}\n`);
			console.log(user.orders[i]);
			if (user.orders[i]._id === req.params.id) {
				console.log('\n Right before splice \n')
				user.orders.splice(i, 1);
				console.log('\n Right after splice \n')
			}
		};

		// save
		return user.save()
		.then(user => true)
		.catch(err => err.message);
	});

	// check if user is updated
	if (isUserUpdated !== true) {
		return res.send({message : isUserUpdated});
	}

	// isProductUpdated
	let isProductUpdated = await Product.findById(req.orders.productId)
	.then(product => {

		// log info to check
		console.log(product);

		// create an object which will contain the userId of the user who made the order
		for (let i = 0; i < product.orders.length; i++) {
			console.log(`\n${i}\n`);
			console.log(product.orders[i]);
			if (product.orders[i]._id === req.params.id) {
				product.orders.splice(i, 1);
			}
		};

		// save
		return product.save()
		.then(product => true)
		.catch(err => err.message);
	});

	// check if product is updated
	if (isProductUpdated !== true) {
		return res.send({message : isProductUpdated});
	}

	if (isUserUpdated && isProductUpdated) {
		return res.send({message : 'Order Successful!'});
	}
};
