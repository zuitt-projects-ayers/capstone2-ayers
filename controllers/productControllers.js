// import Product model
const Product = require('../models/Product');

// Controllers

// Create Product (Admin Only)
module.exports.createProduct = (req, res) => {

	// log info to check
	console.log(req.body);

	// check if name is already taken
	Product.findOne({name : req.body.name})
	.then(result => {

		if (result !== null && result.name === req.body.name) {
			console.log("Product Name has already been used.")
			return res.send('Product Name has already been used.');

		} else {
			// create new product
			if (req.body.publishedOn !== undefined) {
				let newProduct = new Product({
					name : req.body.name,
					description : req.body.description,
					price : req.body.price,
					publishedOn : new Date(req.body.publishedOn)
				});

				newProduct.save()
				.then(product => res.send(product))
				.catch(err => res.send(err));

			} else {
				let newProduct = new Product({
					name : req.body.name,
					description : req.body.description,
					price : req.body.price
				});

				newProduct.save()
				.then(product => res.send(product))
				.catch(err => res.send(err));
			}
		};
	})
	.catch(err => res.send(err));
};


// Get All Products (Admin Only)
module.exports.getAllProducts = (req, res) => {

	// log info to check
	console.log(req.user);

	// find all products (no criteria)
	Product.find({})
	.then(result => res.send(result))
	.then(err => res.send(err));
};


// Archive Product by ID (Admin Only)
module.exports.archiveProduct = (req, res) => {

	// log info to check
	console.log(req.params.id)
	console.log(req.user);

	// create updates object
	let updates = {
		isActive : false
	};

	// find by id and update
	Product.findByIdAndUpdate(req.params.id, updates, {new:true})
	.then(updatedProduct => res.send(updatedProduct))
	.catch(err => res.send(err));
};


// Activate Product by ID (Admin Only)
module.exports.activateProduct = (req, res) => {

	// log info to check
	console.log(req.params.id)
	console.log(req.user);

	// create updates object
	let updates = {
		isActive : true
	};

	// find by id and update
	Product.findByIdAndUpdate(req.params.id, updates, {new:true})
	.then(updatedProduct => res.send(updatedProduct))
	.catch(err => res.send(err));
};


// Get Active Products
module.exports.getActiveProducts = (req, res) => {

	// log info to check
	console.log(req.user);

	// find all products (no criteria)
	Product.find({isActive : true})
	.then(result => res.send(result))
	.catch(err => res.send(err));
};


// Get Single Active Product by ID
module.exports.getSingleActiveProduct = (req, res) => {

	// log info to check
	console.log(req.params.id)
	console.log(req.user);

	// find by id
	Product.findById(req.params.id)
	.then(result => {
		if (result.isActive === false) {
			return res.send('Failed.\nProduct not for sale.');
		} else {
			return res.send(result);
		}
	})
	.catch(err => res.send(err));
};


// Get Single Product by ID (Admin Only)
module.exports.getSingleProduct = (req, res) => {

	// log info to check
	console.log(req.params.id)
	console.log(req.user);

	// find by id
	Product.findById(req.params.id)
	.then(result => res.send(result))
	.catch(err => res.send(err));
};


// Update Product Info (Admin Only)
module.exports.updateProductInfo = (req, res) => {

	// log info to check
	console.log(req.user);
	console.log(req.params.id);
	console.log(req.body);

	// create updates object
	if (req.body.publishedOn != null) {
		let updates = {
			name : req.body.name,
			description : req.body.description,
			price : req.body.price,
			publishedOn : new Date(req.body.publishedOn)
		}

		// find by id and update
		Product.findByIdAndUpdate(req.params.id, updates, {new:true})
		.then(updatedProduct => res.send(updatedProduct))
		.catch(err => res.send(err));

	} else {
		let updates = {
			name : req.body.name,
			description : req.body.description,
			price : req.body.price
		}

		// find by id and update
		Product.findByIdAndUpdate(req.params.id, updates, {new:true})
		.then(updatedProduct => res.send(updatedProduct))
		.catch(err => res.send(err));
	}
};


// Verify Product is Active
module.exports.verifyActive = (req, res, next) => {

	Product.findById(req.body.productId)
	.then(result => {
		if (result.isActive === true) {
			next();

		} else {
			return res.send("Failed.\nProduct not for sale.")
		}
	})
};