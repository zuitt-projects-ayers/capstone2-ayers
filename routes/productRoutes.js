// import and assign express.Router()
const express = require('express');
const router = express.Router();

// import user controllers
const productControllers = require("../controllers/productControllers");

// import verify and verifyAdmin from auth
const auth = require("../auth");
const {verify, verifyAdmin} = auth;

// Routes

// Create Product (Admin Only)
router.post('/createProduct', verify, verifyAdmin, productControllers.createProduct);

// Get All Products (Admin Only)
router.get('/getAllProducts', verify, verifyAdmin, productControllers.getAllProducts);

// Archive Product by ID (Admin Only)
router.put('/archiveProduct/:id', verify, verifyAdmin, productControllers.archiveProduct);

// Activate Product by ID (Admin Only)
router.put('/activateProduct/:id', verify, verifyAdmin, productControllers.activateProduct);

// Get Active Products
router.get('/getActiveProducts', verify, productControllers.getActiveProducts);

// Get Single Active Product by ID
router.get('/getSingleActiveProduct/:id', verify, productControllers.getSingleActiveProduct);

// Get Single Product by ID (Admin Only)
router.get('/getSingleProduct/:id', verify, verifyAdmin, productControllers.getSingleProduct);

// Update Product Info (Admin Only)
router.put('/updateProductInfo/:id', verify, verifyAdmin, productControllers.updateProductInfo);

// export
module.exports = router;