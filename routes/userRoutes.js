// import and assign express.Router()
const express = require('express');
const router = express.Router();

// import user controllers
const userControllers = require("../controllers/userControllers");

// import PRODUCT controllers for verifyActive
const productControllers = require("../controllers/productControllers");
const {verifyActive} = productControllers;

// import verify and verifyAdmin from auth
const auth = require("../auth");
const {verify, verifyAdmin} = auth;

// Routes

// Register User
router.post("/registerUser", userControllers.registerUser);

// Login User
router.post("/loginUser", userControllers.loginUser);

// Set User as Admin (Admin Only)
router.put("/setAdmin/:id", verify, verifyAdmin, userControllers.setAdmin);

// Get All Users (Admin Only)
router.get("/getAllUsers", verify, verifyAdmin, userControllers.getAllUsers);

// Get Single User (Admin Only)
router.get("/getSingleUser/:id", verify, verifyAdmin, userControllers.getSingleUser);

// Get User's Info (Admin Only)
router.get("/getInfo", verify, userControllers.getInfo);

// Update User's Info
router.put("/updateUserInfo", verify, userControllers.updateUserInfo);

// Make an Order (Regular User Only)
router.post("/makeOrder", verify, verifyActive, userControllers.makeOrder);

// Get User's Orders
router.get("/getOrders", verify, userControllers.getOrders);

// Get All Orders (Admin Only)
router.get("/getAllOrders", verify, verifyAdmin, userControllers.getAllOrders);

// Cancel User's Order (not working)
// router.put("/cancelOrder/:id", verify, userControllers.cancelOrder);

// export router
module.exports = router;